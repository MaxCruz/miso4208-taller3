#!/bin/bash

usage_help="Usage: collector.sh [ui|headless] [command]"

case "$1" in 
	ui|headless)
	if [ ! -d "$1" ]
	then
		mkdir $1
	fi
	path="$1"
	;;
        *)
	echo $usage_help
	exit 1
esac

if [ -z "$2" ]
then
	echo $usage_help
	exit 1
fi

framework=$2
time_command="/usr/bin/time -v -o "

for file in {1..20}
do
	command="$time_command $path/$file.txt $framework"
	echo "$command"
	eval $command
done
