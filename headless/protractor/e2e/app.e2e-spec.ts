import { TourOfHeroesPage } from './app.po';

describe('Tour of heroes, dashboard page', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage();
  });

  it('should display top 4 heroes', () => {
    page.navigateTo();
    expect(page.getTop4Heroes()).toEqual(['Mr. Nice', 'Narco', 'Bombasto', 'Celeritas']);
  });

  it('should search hero', () => {
    page.navigateTo();
    expect(page.searchHeroName('IQ')).toEqual('Dr IQ');
  });

  it('should navigate to heroes', () => {
    page.navigateToHeroes();
    expect(page.getAllHeroes().count()).toBe(11);
  });

  it('should navigate to hero from top', () => {
    page.navigateTo();
    page.navigateToHeroFromTop('Mr. Nice');
    expect(page.getHeroDetailTitle()).toEqual(['Mr. Nice details!']);
  });

  it('should navigate to hero from search', () => {
    page.navigateTo();
    page.navigateToHeroFromSearch('Mr. Nice')    
    expect(page.getHeroDetailTitle()).toEqual(['Mr. Nice details!']);
  });

});

describe('Tour of heroes, heroes page', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateToHeroes();
  });

  it('should add a new hero', () => {
    const currentHeroes = page.getAllHeroes().count();
    page.enterNewHeroInInput('My new Hero');
    expect(page.getAllHeroes().count()).toBe(currentHeroes.then(n => n + 1));
  });

  it('should delete a hero', () => {
    const currentHeroes = page.getAllHeroes().count();
    page.deleteHero('My new Hero');
    expect(page.getAllHeroes().count()).toBe(currentHeroes.then(n => n - 1));
  });

  it('should navigate to hero from list', () => {
    page.navigateToHeroDetail('Mr. Nice');
    expect(page.getHeroDetailTitle()).toEqual(['Mr. Nice details!']);  
  })

  it('should edit a hero', () => {
    const currentHeroId = page.getHeroId('Narco');
    page.navigateToHeroDetail('Narco');
    page.editHero('Batman');
    page.navigateToHeroes();
    expect(page.getHeroId('Batman')).toEqual(currentHeroId);
  });

});
