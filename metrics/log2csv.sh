#! /bin/bash

for file in *.txt
do
	awk 'NR==4||NR==5||NR==10' $file | awk '{print $NF}' | paste -sd "," -
done
