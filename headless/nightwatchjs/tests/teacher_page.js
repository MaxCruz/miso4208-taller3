module.exports = { 
  'Los estudiantes teacher page': function(browser) {
    browser
      .url('https://losestudiantes.co/')
      .click('.botonCerrar')
      .pause(1000)
      .waitForElementVisible('.profesor', 4000)
      .click('.profesor > a')
      .pause(1000)
      .assert.visible('.nombreProfesor')
      .end();
  }
};
